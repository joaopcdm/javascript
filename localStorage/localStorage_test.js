//adicionar_localStorage();

//obter_localStorage();

function obter_localStorage(){

	if (localStorage.getItem("pessoa")) {

		//Se existe a pessoa no localStorage
		var pessoa = JSON.parse(localStorage.getItem("pessoa"));
		var nome = localStorage.getItem("nome");

		console.log(nome);
		console.log(pessoa);

	} else{
		console.log('Não existem dados no localStorage :(');
	}
}

function adicionar_localStorage(){

	var pessoa = {
		nome: "João",
		idade: "15",
		sexo: "Masculino"
	}

	var nome = "Leandro";

	localStorage.setItem("pessoa", JSON.stringify(pessoa));
	localStorage.setItem("nome", nome);
}