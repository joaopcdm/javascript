== // Igual - Valores são iguais?
=== //idêntico - São iguais e do mesmo tipo?
!= // Diferente
<> // Diferente
!== // Não idênticos - São diferentes e ou de tipos diferentes?
< // Menor que
> // Maior que
<= // Menor ou igual
>= // Maior ou igual
&& // Nome: E. Função: Verdadeiro se todas expressões forem verdadeiras.
|| // Nome: Ou. Função: Verdadeiro de pelo menos uma das expressões for verdadeira.
! // Nome: Negação. Função: Inverte o resultado da expressão.
+ // Adição - Soma valores.
- // Subtração - Diferença entre valores.
* // Multiplicação - Produto dos valores.
/ /// Divisão - Quociente dos valores.
% // Módulo - Resto existente em uma operação de divisão.
++ // Incremento - Pré/pós incremento.
-- // Decremento - Pré/pós decremento. 

	//PHP

AND // Verdadeiro se todas as expressões forem verdadeiras.
&& // Nome: E. Função: Verdadeiro se todas expressões forem verdadeiras.
OR // Verdadeiro se pelo menos uma das expressões for verdadeira.
|| // Nome: Ou. Função: Verdadeiro de pelo menos uma das expressões for verdadeira.
XOR // Verdadeiro apenas se uma das expressões for verdadeira.
! // Nome: Negação. Função: Inverte o resultado da expressão.

